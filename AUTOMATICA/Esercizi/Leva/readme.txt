Esercizi (PDF)
Sistemi dinamici (analisi nel dominio del tempo):
sdt1.pdf (sdt1s.pdf), sdt2.pdf (sdt2s.pdf), sdt3.pdf (sdt3s.pdf), sdt4.pdf (sdt4s.pdf),
sdt5.pdf (sdt5s.pdf), sdt6.pdf (sdt6s.pdf), sdt7.pdf, sdt8.pdf, sdt9.pdf (sdt9s.pdf)

Funzione di trasferimento:
fdt1.pdf (fdt1s.pdf), fdt2.pdf (fdt2s.pdf), fdt3.pdf (fdt3s.pdf)

Schemi a blocchi:
sb1.pdf (sb1s.pdf), sb2.pdf (sb2s.pdf), sb3.pdf (sb3s.pdf), sb4.pdf (sb4s.pdf),
sb5.pdf (sb5s.pdf), sb6.pdf (sb6s.pdf)

Sistemi dinamici analisi nel dominio della frequenza:
sdf1.pdf (sdf1s.pdf), sdf2.pdf (sdf2s.pdf), sdf3.pdf (sdf3s.pdf)

Analisi di sistemi retroazionati:
sr1.pdf, sr2.pdf (sr2s.pdf), sr3.pdf (sr3s.pdf)

Progetto del regolatore:
pr1.pdf, pr2.pdf

Controllori industriali:
ci1.pdf, ci2.pdf, ci3.pdf, ci4.pdf

Controllo digitale:
cd1.pdf, cd2.pdf

Tips orale con Piazza.

- L'esame orale dura una mezzoretta

- Portatevi una penna! O scatenerà la sua furia su di voi

- Potete portarvi un argomento a piacere, potete partire con quello, se non vi chiede lui di esporlo chiedetelo voi

- Non potete portarvi un argomento lunghissimo e pensare di dirlo tutto per prendere tempo, ve lo taglierà. 10-15 minuti sono il vostro tempo massimo

- Esempi di argomenti a piacere: TCL e risultati, Stimatori e loro proprietà, Reliability e Hazard function, qualsiasi argomento delle 11 domande

- L'orale è costituito da domande che si è preparato prima e che vi chiederà, vi chiederà sicuramente almeno 1 delle 11 domande

- Le altre domande vengono da una pool di domande, in questa cartella cè un file che ha inviato lui (Domande orale 2023) con alcune delle domande che provengono dalla sua pool di domande,
fatevele tutte, perché molte domande vengono da quel foglio

- Tra le 11 domande non cè la distribuzione di Poisson, imparatela bene lo stesso, la chiede e chiede la relazione con l'esponenziale

- Solitamente chiede almeno un esercizio, i più gettonati sono sulle distribuzioni di Poisson ed Esponenziale, Verifica di ipotesi e Stima intervallare

- È una persona scorbutica, potreste avere l'impressione di star andando male per il modo in cui vi risponde, ma non necessariamente è così. Cercate di non farvi influenzare

- Altri argomenti che chiede che non sono su quel foglio:
	Verifica di ipotesi sulla varianza, media conosciuta o sconosciuta (è una Chi quadro, dipendentemente dalla conoscenza nella media la formula è leggermente diversa)
	R^2, cosè e cosa indica
	Test di Pearson (personalmente vi consiglio di leggervi questa pagina e fare l'esercizio per capirlo, https://meetheskilled.com/test-quadro-test-di-indipendenza-due-variabili/)
	Assiomi di Poisson
	Non l'ho mai sentito chiedere Fisher e Kolmogorov


- Infine, di seguito, quasi tutte le domande che ha fatto nell'appello di Gennaio 2023, ogni gruppo di domande è un singolo orale:


Dimostrazione che la somma degli scarti al quadrato fa zero.
Quartili, definizione e formula per trovarli
Definizione di boxplot
Distribuzione t-student. Media e altre cose sempre riguardo la t-student come i gradi di libertà necessari per avere che la t-student assomigli una normale 0,1



Inizio esame: vede come sono andate le prove in itinere
Prende le 11 domande
Argomento a piacere.
Istogramma: caratteristiche delle canne (adiacenti e area)
Definizione di frequenza assoluta e relativa



Inizio esame: ti dà un foglio su un argomento su cui eri debole alle pi
Ordinare dataset, frequenza assoluta e relativa, definizione di mediana, elementi che servono per il boxplot, costruzione del boxplot.
Vettori gaussiani: vettore media, matrice di covarianza: sapere bene cosa sono gli indici, funzione di densità data la matrice di covarianza.
11 domande: test d’ipotesi bilatero (statistica test, regione critica, stimatore per la media, espressione di s quadrato, che è la varianza campionaria,), cos’è uno stimaatore corretto. Disuguaglianza di Chevicev. Media della statistsica test del test d’ipotesi bilatero. Definizione di alpha (è una probabilità). 
Indipendenza di eventi, definizione e scrittura insiemistica. E probabilità
Legge di Bayes (è una formula che permette di invertire la probabilità degli eventi condizionanti) -> (cause ed effetti). A cosa serve la legge di bayes e scrittura della cosa delle cause/effetti in matematichese.
Definizione di partizione di un evento



VA assolutamente continua, legame con la funzione di densità e la funzione cumulativa. 
Unità di misura della funzione cumulativa (è adimensionale), e della funzione di densità (1/metro).
Definizione di ripartizione dell’evento certo.
Test di Perason 
Test d’ipotesi non parametrico e la sua statistica test.
Popolazione discreta con distribuzione bernoulli, come si fa a stimare p? Statistica test
Bias



Distribuzione della regressione normale
Cosa ci rassicura riguardo la linerarità della regressione? (r^2)
Test di Pearson
Intervallo di confidenza per la varianza di una popolazione gaussiana nota la media.
Distribuzione dell’intervallo di confidenza.
Quadrato di una va normale?



Argomento a scelta
Disuguaglianza di markov e dimostrazione
Test ipostesi e statistica di Test con varianza ignota
Distribuzione di stastistica di Test
Quante code ha la regione critica
Quantile per la significatività 0.05



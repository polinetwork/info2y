//--------------------------------------------------------------------------------------------------------
//--
//--   Davide Galeone - Matricola: 847742 - 28 Ott 2017
//--   
//--   Note e considerazioni:
//--   
//--   Le funzioni matematiche math_resto(a,b) e math_div(a,b)
//--   insieme alla funzione print_number(numero) permettono di effettuare l'output
//--   di numeri interi maggiori di 9 che richiedono l'output di più caratteri ASCII
//--   
//--   L'algoritmo quindi supporta anche l'output di valori di MIN, NMIN, MAX, NMAX maggiori di 9
//--   
//--------------------------------------------------------------------------------------------------------

.constant
	OBJREF 0
	R_VOID 0
.end-constant

.main
	
	//Dichiarazione variabili
	.var		
		l_max
		n_max

		l_min
		n_min		

		l_tmp

		elem

		first
	.end-var

//--------------------------------------
//--   Caricamento Dati in PILA		  --
//--   Carattere terminatore 0x00	  --
//--   Carattere separatore	 0xFF	  --
//--------------------------------------

	BIPUSH 0x00
	BIPUSH 0xAA
	BIPUSH 0xAA
	BIPUSH 0xFF
	BIPUSH 0x01
	BIPUSH 0x02
	BIPUSH 0x03
	BIPUSH 0xFF
	BIPUSH 0xBB
	BIPUSH 0xBB

//--------------------------------------
//--------------------------------------

	//Testo

	BIPUSH 67
	OUT
	BIPUSH 65
	OUT
	BIPUSH 76
	OUT
	BIPUSH 67
	OUT
	BIPUSH 79
	OUT
	BIPUSH 76
	OUT
	BIPUSH 79
	OUT
	BIPUSH 46
	OUT

//--------------------------------------

	//inizializzo first
	BIPUSH 1
	ISTORE first

	//inizializzo l_tmp
	BIPUSH 0
	ISTORE l_tmp

main_ciclo:
	
	//prendo il dato dallo stack
	ISTORE elem

	//- verifico se è il terminatore
	ILOAD elem
	BIPUSH 0x00
	IF_ICMPEQ main_cond_TERM_if
	GOTO main_cond_TERM_endif_noaction
	main_cond_TERM_if:
	//-> (begin) se è il terminatore
	//
		ILOAD first
		BIPUSH 0
		IF_ICMPEQ main_TERM_first_else
		//-> (begin) if first
		//
			//assegno la lunghezza a min
			ILOAD l_tmp
			ISTORE l_min

			//assegno 1 a min
			BIPUSH 1
			ISTORE n_min

			//assegno la lunghezza a max
			ILOAD l_tmp
			ISTORE l_max

			//assegno 1 a max
			BIPUSH 1
			ISTORE n_max

			//azzero l_tmp
			BIPUSH 0
			ISTORE l_tmp
			
			//setta first a zero
			BIPUSH 0
			ISTORE first
			//salta a endif
			GOTO main_TERM_first_endif
		//
		//-> (end) if first
		main_TERM_first_else:
		//-> (begin) else
		//
			//--- MIN ---

			//verifico se è l_tmp < l_min
			ILOAD l_tmp
			ILOAD l_min			
			ISUB
			IFLT main_TERM_lTmp_min_lMin
			GOTO main_TERM_comp_minor_end
			main_TERM_lTmp_min_lMin:
			//-> (begin)
			//
				ILOAD l_tmp
				ISTORE l_min
				BIPUSH 1
				ISTORE n_min
			//
			//-> (end)
			GOTO main_TERM_comp_min_end

			main_TERM_comp_minor_end:

			//verifico se è l_tmp = l_min
			ILOAD l_tmp
			ILOAD l_min
			IF_ICMPEQ main_TERM_lTmp_eq_lMin
			GOTO main_TERM_comp_min_end
			main_TERM_lTmp_eq_lMin:
			//-> (begin)
			//
				IINC n_min 1
			//
			//-> (end)

			main_TERM_comp_min_end:

			//--- MAX ---

			//verifico se è l_tmp > l_max
			ILOAD l_max
			ILOAD l_tmp			
			ISUB
			IFLT main_TERM_lTmp_mag_lMax
			GOTO main_TERM_comp_mag_end
			main_TERM_lTmp_mag_lMax:
			//-> (begin)
			//
				ILOAD l_tmp
				ISTORE l_max
				BIPUSH 1
				ISTORE n_max
			//
			//-> (end)
			GOTO main_TERM_comp_max_end

			main_TERM_comp_mag_end:

			//verifico se è l_tmp = l_max
			ILOAD l_max
			ILOAD l_tmp
			IF_ICMPEQ main_TERM_lTmp_eq_lMax
			GOTO main_TERM_comp_max_end
			main_TERM_lTmp_eq_lMax:
			//-> (begin)
			//
				IINC n_max 1
			//
			//-> (end)

			main_TERM_comp_max_end:

			//IMPORTANTE: azzero l_tmp
			//BIPUSH 0
			//ISTORE l_tmp
		//
		//-> (end) first else
		main_TERM_first_endif:
	//
	//-> (end) se è il terminatore
	GOTO main_cond_TERM_endif

main_cond_TERM_endif_noaction:

	//- verifico se è il separatore
	ILOAD elem
	BIPUSH 0xFF
	IF_ICMPEQ main_cond_SEP_if
	GOTO main_cond_SEP_endif_noaction
	main_cond_SEP_if:
	//-> (begin) se è il separatore
	//
		ILOAD first
		BIPUSH 0
		IF_ICMPEQ main_SEP_first_else
		//-> (begin) if first
		//
			//assegno la lunghezza a min
			ILOAD l_tmp
			ISTORE l_min

			//assegno 1 a min
			BIPUSH 1
			ISTORE n_min

			//assegno la lunghezza a max
			ILOAD l_tmp
			ISTORE l_max

			//assegno 1 a max
			BIPUSH 1
			ISTORE n_max

			//azzero l_tmp
			BIPUSH 0
			ISTORE l_tmp
			
			//setta first a zero
			BIPUSH 0
			ISTORE first
			//salta a endif
			GOTO main_SEP_first_endif
		//
		//-> (end) if first
		main_SEP_first_else:
		//-> (begin) else
		//
			//--- MIN ---

			//verifico se è l_tmp < l_min
			ILOAD l_tmp
			ILOAD l_min			
			ISUB
			IFLT main_SEP_lTmp_min_lMin
			GOTO main_SEP_comp_minor_end
			main_SEP_lTmp_min_lMin:
			//-> (begin)
			//
				ILOAD l_tmp
				ISTORE l_min
				BIPUSH 1
				ISTORE n_min
			//
			//-> (end)
			GOTO main_SEP_comp_min_end

			main_SEP_comp_minor_end:

			//verifico se è l_tmp = l_min
			ILOAD l_tmp
			ILOAD l_min
			IF_ICMPEQ main_SEP_lTmp_eq_lMin
			GOTO main_SEP_comp_min_end
			main_SEP_lTmp_eq_lMin:
			//-> (begin)
			//
				IINC n_min 1
			//
			//-> (end)

			main_SEP_comp_min_end:

			//--- MAX ---

			//verifico se è l_tmp > l_max
			ILOAD l_max
			ILOAD l_tmp			
			ISUB
			IFLT main_SEP_lTmp_mag_lMax
			GOTO main_SEP_comp_mag_end
			main_SEP_lTmp_mag_lMax:
			//-> (begin)
			//
				ILOAD l_tmp
				ISTORE l_max
				BIPUSH 1
				ISTORE n_max
			//
			//-> (end)
			GOTO main_SEP_comp_max_end

			main_SEP_comp_mag_end:

			//verifico se è l_tmp = l_max
			ILOAD l_max
			ILOAD l_tmp
			IF_ICMPEQ main_SEP_lTmp_eq_lMax
			GOTO main_SEP_comp_max_end
			main_SEP_lTmp_eq_lMax:
			//-> (begin)
			//
				IINC n_max 1
			//
			//-> (end)

			main_SEP_comp_max_end:

			//IMPORTANTE: azzero l_tmp
			BIPUSH 0
			ISTORE l_tmp
		//
		//-> (end) first else
		main_SEP_first_endif:
	//
	//-> (end) se è il separatore
	GOTO main_cond_SEP_endif

main_cond_SEP_endif_noaction:
	
	//altrimenti se è un elemento
	IINC l_tmp 1

//skip del ciclo per separatore
main_cond_SEP_endif:

//output puntini
BIPUSH 46
OUT

//ritorno in testa al ciclo
GOTO main_ciclo

//uscita ciclo per terminatore
main_cond_TERM_endif:



//> (begin) OUTPUT TESTO
	
	BIPUSH 10
	OUT
	BIPUSH 10
	OUT

	//A) MIN: ---, NMIN: ---
	BIPUSH 65
	OUT
	BIPUSH 41
	OUT
	
	BIPUSH 32
	OUT

	BIPUSH 77
	OUT
	BIPUSH 73
	OUT	
	BIPUSH 78
	OUT
	BIPUSH 58
	OUT
	
	BIPUSH 32
	OUT

	//call function print_number(numero)
	LDC_W OBJREF
    ILOAD l_min
    INVOKEVIRTUAL print_number
    POP

    BIPUSH 44
	OUT

	BIPUSH 32
	OUT

	BIPUSH 78
	OUT
	BIPUSH 77
	OUT
	BIPUSH 73
	OUT	
	BIPUSH 78
	OUT
	BIPUSH 58
	OUT
	
	BIPUSH 32
	OUT

	//call function print_number(numero)
	LDC_W OBJREF
    ILOAD n_min
    INVOKEVIRTUAL print_number
    POP

    BIPUSH 10
	OUT

	//B) MAX: -, NMAX: -
	BIPUSH 66
	OUT
	BIPUSH 41
	OUT
	
	BIPUSH 32
	OUT

	BIPUSH 77
	OUT
	BIPUSH 65
	OUT	
	BIPUSH 88
	OUT
	BIPUSH 58
	OUT
	
	BIPUSH 32
	OUT

	//call function print_number(numero)
	LDC_W OBJREF
    ILOAD l_max
    INVOKEVIRTUAL print_number
    POP

    BIPUSH 44
	OUT

	BIPUSH 32
	OUT

	BIPUSH 78
	OUT
	BIPUSH 77
	OUT
	BIPUSH 65
	OUT	
	BIPUSH 88
	OUT
	BIPUSH 58
	OUT
	
	BIPUSH 32
	OUT

	//call function print_number(numero)
	LDC_W OBJREF
    ILOAD n_max
    INVOKEVIRTUAL print_number
    POP

//> (end) OUTPUT TESTO

    HALT    
.end-main



//-----------------------
//-- METODI MATEMATICI --
//-----------------------

//- end:Divisione ------------------------------
.method math_div(a, b)
	//variabili
	.var
		count
	.end-var

	//- azzera count
	BIPUSH 0
	ISTORE count

	//scrive a sullo stack per divisione
	ILOAD a

math_div_ciclo_i:
	ILOAD b
	ISUB
	DUP
	IFLT math_div_ciclo_f
	IINC count 1
	GOTO math_div_ciclo_i
math_div_ciclo_f:

	POP
	ILOAD count

	IRETURN
.end-method
//- end:Divisione ------------------------------

//- start:Resto ------------------------------
.method math_resto(a, b)

	//scrive a sullo stack per divisione
	ILOAD a
	
math_resto_ciclo_i:
	
	ILOAD b
	ISUB

	DUP
	IFLT math_resto_ciclo_f
	GOTO math_resto_ciclo_i
math_resto_ciclo_f:
	
	ILOAD b
	IADD

	IRETURN
.end-method
//- end:Resto ------------------------------



//-------------------
//-- METODI OUTPUT --
//-------------------

//- end:StampaNumero ------------------------------
.method print_number(numero)
	//variabili
	.var
		temp_sub
		count
		count2
		resto
	.end-var

	//- azzera count
	BIPUSH 0
	ISTORE count

	//- azzera count2
	BIPUSH 0
	ISTORE count2

print_number_ciclo_i:
	
	//function resto
	LDC_W OBJREF
    ILOAD numero
    BIPUSH 10
    INVOKEVIRTUAL math_resto

	//e ritrovo il resto sullo stack

	//lascio il resto sulla stack, per dopo dove leggerò in ordine invertito

    //function divisione
	LDC_W OBJREF
    ILOAD numero
    BIPUSH 10
    INVOKEVIRTUAL math_div

    //e ritrovo il quoto sullo stack

    ISTORE numero

    IINC count 1
    
    ILOAD numero
    IFEQ print_number_ciclo_f
    GOTO print_number_ciclo_i
print_number_ciclo_f:

print_number_ciclo2_i:
	ILOAD count
	ILOAD count2
	ISUB
	IFEQ print_number_ciclo2_f

	BIPUSH 48
	IADD
	OUT

	IINC count2 1
	GOTO print_number_ciclo2_i
print_number_ciclo2_f:

	LDC_W R_VOID

	IRETURN
.end-method
//- end:StampaNumero ------------------------------


